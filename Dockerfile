FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/*.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/*.jar"]
