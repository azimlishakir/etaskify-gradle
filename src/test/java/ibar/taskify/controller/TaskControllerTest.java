package ibar.taskify.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ibar.taskify.dto.TaskDto;
import ibar.taskify.enums.MailSendStatus;
import ibar.taskify.model.Task;
import ibar.taskify.model.User;
import ibar.taskify.service.Impl.TaskServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)

public class TaskControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final MailSendStatus DUMMY_STATUS = MailSendStatus.NOT_SENDED;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskServiceImpl taskService;







    private TaskDto taskDto;
    private Task task;
    private User user;

    @BeforeEach
    void setUp(){
        taskDto = TaskDto
                .builder()
                .assignIds(List.of(DUMMY_ID))
                .build();

        user = User
                .builder()
                .id(DUMMY_ID)
                .build();

        task = Task
                .builder()
                .reporterUser(user)
                .assignId(DUMMY_ID)
                .sendStatus(DUMMY_STATUS)
                .build();
    }

    @Test
    void createTask() throws Exception{
        doNothing().when(taskService).createTask(any(),anyString());

        mvc.perform(post("/create-task")
                .content(objAsJson(taskDto))
                .header("Auth","token")
                .contentType(APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String objAsJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }




}