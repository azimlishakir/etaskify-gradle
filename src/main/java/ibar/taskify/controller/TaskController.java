package ibar.taskify.controller;

import ibar.taskify.dto.TaskDto;
import ibar.taskify.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/create-task")
    @PreAuthorize("@authServiceImpl.hasValidToken(#token)")
    public ResponseEntity createTask(@RequestBody TaskDto taskDto,
                                     @RequestHeader("Auth") @NotBlank String token) {
        taskService.createTask(taskDto, token);
        return ResponseEntity.ok().build();
    }


}
