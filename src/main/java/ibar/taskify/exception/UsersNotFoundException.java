package ibar.taskify.exception;

public class UsersNotFoundException extends NotFoundException  {
    private static final long serialVersionUID = 58432132487812L;

    public static final String MESSAGE = "Users Id %s does not exist.";

    public UsersNotFoundException(String ex) {
        super(String.format(MESSAGE, ex));
    }

}
