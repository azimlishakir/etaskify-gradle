package ibar.taskify.exception;

public class AuthsException extends NotFoundException {
    private static final long serialVersionUID = 58432132487812L;

    public static final String MESSAGE = "Auth Id %s does not exist.";

    public AuthsException(String ex) {
        super(String.format(MESSAGE, ex));
    }
}
