package ibar.taskify.repository;

import ibar.taskify.enums.MailSendStatus;
import ibar.taskify.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task,Long> {
    List<Task> findBySendStatus(MailSendStatus mailSendStatus);
}
