package ibar.taskify.repository;

import ibar.taskify.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role,Long> {
    Role findByRoleName(String roleName);
}
