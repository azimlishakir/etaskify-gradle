package ibar.taskify.service;

import ibar.taskify.dto.TaskDto;

public interface TaskService {

    void createTask(TaskDto taskDto, String token);

}
