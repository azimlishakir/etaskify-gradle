package ibar.taskify.service;

import ibar.taskify.dto.RegistrationDto;

public interface RegistrationService {

    void signUp(RegistrationDto registrationDto);
}
