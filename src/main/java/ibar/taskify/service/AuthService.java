package ibar.taskify.service;

import ibar.taskify.dto.AuthDto;
import ibar.taskify.dto.AuthRespDto;

public interface AuthService {

    AuthRespDto login(AuthDto authDto);

    boolean hasValidToken(String token);

}
