package ibar.taskify.service;

import ibar.taskify.dto.UserDto;

public interface UserService {
    void createUser(UserDto userDto, String token);
}
