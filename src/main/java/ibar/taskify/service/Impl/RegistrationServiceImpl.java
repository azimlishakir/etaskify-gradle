package ibar.taskify.service.Impl;

import ibar.taskify.dto.RegistrationDto;
import ibar.taskify.mapper.UserMapper;
import ibar.taskify.model.Role;
import ibar.taskify.model.User;
import ibar.taskify.repository.RoleRepository;
import ibar.taskify.repository.UserRepository;
import ibar.taskify.service.RegistrationService;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    public RegistrationServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void signUp(RegistrationDto registrationDto) {
        User user = UserMapper.INSTANCE.toEntity(registrationDto);
        Role role = roleRepository.findByRoleName("ADMIN");
        System.out.println(role.getRoleName());
        user.setRole(role);
        userRepository.save(user);
    }
}
