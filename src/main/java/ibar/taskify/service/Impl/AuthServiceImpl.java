package ibar.taskify.service.Impl;


import ibar.taskify.dto.AuthDto;
import ibar.taskify.dto.AuthRespDto;
import ibar.taskify.exception.UsersNotFoundException;
import ibar.taskify.model.User;
import ibar.taskify.repository.UserRepository;
import ibar.taskify.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public AuthRespDto login(AuthDto authDto) {

      User user = userRepository.findByUsernameAndPassword(authDto.getUsername(), authDto.getPassword())
                .orElseThrow(()->new UsersNotFoundException("user not found"));

      String token = UUID.randomUUID().toString();
      user.setToken(token);
      userRepository.save(user);

      return AuthRespDto.builder().token(token).build();
    }

    @Override
    public boolean hasValidToken(String token) {
        return userRepository
                .findByToken(token).isPresent();
    }
}
