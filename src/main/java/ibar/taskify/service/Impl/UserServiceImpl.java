package ibar.taskify.service.Impl;


import ibar.taskify.dto.UserDto;
import ibar.taskify.exception.AuthsException;
import ibar.taskify.exception.UsersNotFoundException;
import ibar.taskify.mapper.UserMapper;
import ibar.taskify.model.Customer;
import ibar.taskify.model.Role;
import ibar.taskify.model.User;
import ibar.taskify.repository.RoleRepository;
import ibar.taskify.repository.UserRepository;
import ibar.taskify.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void createUser(UserDto userDto, String token) {
        User user = UserMapper.INSTANCE.toEntity(userDto);
        Role role = roleRepository.findByRoleName("USER");
        user.setRole(role);
        user.setCustomer(getCustomer(token));
        userRepository.save(user);
    }

    private Customer getCustomer(String token) {
        User user = userRepository
                .findByToken(token)
                .orElseThrow(()->new UsersNotFoundException("user not found"));

        if (!user.getRole().getRoleName().equals("ADMIN")) {
            throw new AuthsException("access denied");
        }

        return user.getCustomer();
    }
}
