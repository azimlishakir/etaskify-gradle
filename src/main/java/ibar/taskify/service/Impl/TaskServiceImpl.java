package ibar.taskify.service.Impl;

import ibar.taskify.dto.TaskDto;
import ibar.taskify.enums.MailSendStatus;
import ibar.taskify.mapper.TaskMapper;
import ibar.taskify.model.Task;
import ibar.taskify.repository.TaskRepository;
import ibar.taskify.repository.UserRepository;
import ibar.taskify.service.TaskService;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private UserRepository userRepository;

    public TaskServiceImpl(TaskRepository taskRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void createTask(TaskDto taskDto, String token) {
        taskDto.getAssignIds().forEach(id -> {
            Task task = TaskMapper.INSTANCE.toEntity(taskDto);
            task.setReporterUser(userRepository.findByToken(token).get());
            task.setAssignId(id);
            task.setSendStatus(MailSendStatus.NOT_SENDED);
            task.setDescription(taskDto.getDescription());
            task.setTitle(taskDto.getTitle());
            task.setDeadline(taskDto.getDeadline());
            taskRepository.save(task);
        });

    }
}
